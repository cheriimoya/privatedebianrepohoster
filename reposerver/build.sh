#!/usr/bin/env bash

sudo docker build . --tag reposerver

sudo docker run --name reposerver-container -ti reposerver

sudo docker rm reposerver-container

