#!/bin/bash

main(){
    local NAME="repo"

    local REMOTE="172.17.0.2::$NAME"
    local LOCAL="/net/mirror/"
    
    rsync -av $REMOTE $LOCAL

    mkdir -p /net/mirror/myrepo/dists/$NAME/main/binary-amd64/
    mkdir -p /net/mirror/myrepo/pool/main/

    mv /net/mirror/$NAME/*.deb /net/mirror/myrepo/pool/main/
    rmdir /net/mirror/$NAME

    PACKAGES="/net/mirror/myrepo/dists/$NAME/main/binary-amd64/Packages"

    for FILE in $(find /net/mirror/myrepo/pool/main/ -type f); do
        cat << EOF >> $PACKAGES
Package: $(basename $FILE)
Architecture: all
Version: 0.0.1
Depends:
Filename: pool/main/$(basename $FILE)
Size: $(stat --format=%s $FILE)
MD5sum: $(md5sum $FILE | awk '{ print $1 }')
SHA1: $(sha1sum $FILE | awk '{ print $1 }')
SHA256: $(sha256sum $FILE | awk '{ print $1 }')

EOF
     done

    RELEASE="/net/mirror/myrepo/dists/$NAME/Release"
    
    cat << EOF >> $RELEASE
Origin: MyUbuntu
Label: MyUbuntu
Suite: $NAME
Version: 18.04
Codename: $NAME
Date: Tue, 16 Dec 2018 22:49:23 UTC
Architectures: amd64
Components: main
Description: Ubuntu $NAME 18.04
MD5Sum:
 $(md5sum $PACKAGES | awk '{ print $1 }') $(stat --format=%s $PACKAGES) main/binary-amd64/Packages
SHA1:
 $(sha1sum $PACKAGES | awk '{ print $1 }') $(stat --format=%s $PACKAGES) main/binary-amd64/Packages
SHA256:
 $(sha256sum $PACKAGES | awk '{ print $1 }') $(stat --format=%s $PACKAGES) main/binary-amd64/Packages
EOF

    cd /net/mirror/myrepo/
    python -m SimpleHTTPServer 80

#    echo "To shut down the repo server hit enter"
#    read
}

main
