#!/usr/bin/env bash

sudo docker build . --tag testmachine

sudo docker run --name testmachine-container -ti testmachine

sudo docker rm testmachine-container
