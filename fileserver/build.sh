#!/usr/bin/env bash

sudo docker build . --tag fileserver

sudo docker run --name fileserver-container -ti fileserver

sudo docker rm fileserver-container
