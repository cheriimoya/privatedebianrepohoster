#!/bin/bash

# Start rsync as a daemon
rsync --daemon

# Wait for user to close server
echo "Hit enter to shut down the fileserver"
read
